

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ReceiptAfter.InputParser;
import ReceiptAfter.Order;
import ReceiptAfter.OutputFormater;


public class ReceiptAfter {
  
    
    public static void main(String[] args) {
        Order order = InputParser.ReadOrder("receipt info.txt");
        OutputFormater.PrintToFile("formatted receipt2.txt", order);
    }
}