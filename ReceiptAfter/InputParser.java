/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReceiptAfter;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Kecheor
 */
public class InputParser {
    
    public static Order ReadOrder(String filePath)
    {
        HashMap<String, String> Fields = new HashMap<String, String>();
        ArrayList<String> Goods = new ArrayList<String>();
       
        ReadFile(filePath, Fields, Goods);
                
        Order result = new Order(
                GetCustomer(Fields.get("Customer")),
                Fields.get("Order number"),
                Fields.get("Delivery date"),
                GetCard(Fields.get("Card")),
                Double.parseDouble(Fields.get("Delivery")),
                Double.parseDouble(Fields.get("Voucher"))
        );
        for (String good : Goods)
        {
            result.AddItem(GetOrderItem(good));
        }
        return result;
    }
    
    protected static void ReadFile(String filePath, HashMap<String, String> Fields, ArrayList<String> Goods)
    {
         try {
		Scanner in = new Scanner(new FileReader(filePath));
			
		//Will break in the loop when reached 'Goods' string
                while(in.hasNextLine()) {
                        String line = in.nextLine();
                        if(line.isEmpty())
                            continue;
                         if(line.startsWith("Goods:"))
                            break;
                        String[] pair = line.split(": ");
                       
                        Fields.put(pair[0], pair[1]);
                }
                in.nextLine();
                while(in.hasNextLine()) {
                    String line = in.nextLine();
                    Goods.add(line);
                }
                
            in.close();
	} catch (IOException e) {
		System.err.format("Error reading from file: %s%n", e);
		//return;
	}
    }
    
    protected static Customer GetCustomer(String name)
    {
        String[] nameParts = name.split(" ");
        String prefix = nameParts[nameParts.length-1];
        int i = 0;
        String surname = "";
        while(!nameParts[i].endsWith(",")){
            surname += nameParts[i] + " ";
            i++;
        } 
        surname+=nameParts[i].substring(0, nameParts[i].length()-1);
        i++;
        String firstname = "";
        while(i < nameParts.length - 1) {
            firstname += nameParts[i] + " ";
            i++;
        }    
        firstname = firstname.trim();  
        return new Customer(prefix, firstname, surname);
    }
    
    protected static CreditCard GetCard(String cardInfo)
    {
        int ch = cardInfo.indexOf(' ');
        String type = cardInfo.substring(0, ch);
        String number = cardInfo.substring(ch+1).replace(" ", "");
        return new CreditCard(type, number);
    }
    
    protected static OrderItem GetOrderItem(String itemInfo)
    {
        String parts[] = itemInfo.split(",");
        return new OrderItem(parts[0], Double.parseDouble(parts[2]), Integer.parseInt(parts[1]), Double.parseDouble(parts[3]));
    }
}
