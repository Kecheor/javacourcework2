/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReceiptAfter;

/**
 *
 * @author Kecheor
 */
public class Utils {
    public static byte charToDigit(char c)
    {
        if((int)c < 48 || (int)c > 57)
            throw new IllegalArgumentException("Not a digit");
        return (byte)((int)c - 48);
    }
}
