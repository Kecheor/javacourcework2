package ReceiptAfter;

import java.util.ArrayList;

public class Order {
    public Customer Customer;
    public String Number;
    public String Delivery;
    
    public CreditCard Card;
    public double PickPackDeliverPrice;
    public double Voucher;
    
    public ArrayList<OrderItem> Goods;
    
    public Order(Customer customer,
                 String number,
                 String delivery,
                 CreditCard card,
                 double pickPackDeliver,
                 double voucher)
    {
        Customer = customer;
        Number = number;
        Delivery = delivery;
        Card = card;
        PickPackDeliverPrice = pickPackDeliver;
        Voucher = voucher;
        Goods = new ArrayList<>();
    }
    

    
    public void AddItem(OrderItem item)
    {
        Goods.add(item);
    }
    
    /**
     * @return total cost of good
     */
    public double CostOfGoods()
    {
        double result = 0;
        for (OrderItem item : Goods) {
           result += item.Price;
        }
        return result;
    }
    
    /*
    * @return total charge that cannot be less then zero
    */
    public double TotalCharge()
    {
        double result = CostOfGoods() + PickPackDeliverPrice + Voucher;
        return result < 0 ? 0 : result;
    }
}
