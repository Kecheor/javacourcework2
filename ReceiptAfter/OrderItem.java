package ReceiptAfter;

/**
 * Contains info about order good entry
 * and calculates total price to pay
 */
public class OrderItem {
    public Good Good;
    public int Quantity;
    public double Price;
     
    public OrderItem(String goodName, double goodPrice, int quantity, double price)
    {
        if(quantity < 0)
            throw new IllegalArgumentException("Good quantity cannot be less then zero");
        Good = new Good(goodName, goodPrice);
        Quantity = quantity;
        Price = price;
    }    
}
