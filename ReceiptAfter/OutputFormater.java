/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReceiptAfter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kecheor
 */
public class OutputFormater {
    //"formatted receipt.txt"
    public static void PrintToFile(String filePath, Order order)
    {
        List<String> receipt = Format(order);
        try {
		FileWriter writer =  new FileWriter(filePath);
		for (int i = 0; i < receipt.size(); i++) {
			writer.write(String.format("%s%n", receipt.get(i)));
		}
		writer.close();
	} catch (IOException e) {
		System.err.format("Error writing receipt to disk: %s%n", e);
		return;
	}
    }
    
    
    protected static List<String> Format(Order order)
    {
        List<String> receipt = new ArrayList<String>();
        
        receipt.add("\t\t\tYour receipt from Wellard's organic supermarket");
        receipt.add("");
	receipt.add("");

	receipt.add(String.format("%s %s %s", order.Customer.Prefix, order.Customer.Name, order.Customer.LastName) );
	receipt.add(String.format("%s: %s","Order number", order.Number) ); 
	receipt.add(String.format("%s: %s","Delivery date", order.Delivery) ); 
	receipt.add("Customer Service team: 0345 000 000 (open 8am to 11pm every day) or");
	receipt.add("wellards@wellards.com");
		
	receipt.add("");
        receipt.add("");

	//add breakdown of costs and the total
	receipt.add( String.format("%-30s\t\t£%.2f","Cost of goods", order.CostOfGoods()) );
	receipt.add( String.format("%-30s\t\t£%.2f", "Picking, packing and delivery", order.PickPackDeliverPrice) );
        if(order.Voucher != 0)
            receipt.add( String.format("%-30s\t\t-£%.2f", "Voucher", Math.abs(order.Voucher)) );
	receipt.add( String.format("%-30s\t\t£%.2f", "Total charge", order.TotalCharge()) );
	receipt.add( String.format("%s%nCard: %s Last four digits: %s", "Payment details", order.Card.Type, order.Card.Last4Digits()) );
	receipt.add("");
	receipt.add("");

		
	//add itemised goods bill
	receipt.add( String.format("%-32s%s\t%s\t%s","Item","Quantity","Price","Price to pay (£)") );
	for (OrderItem item : order.Goods) {
		receipt.add( String.format("%-32s%s\t\t%.2f\t%.2f", item.Good.Name, item.Quantity, item.Good.Price, item.Price) );
	}
		
	//add footer
	receipt.add("");
	receipt.add("");
	receipt.add("\tPlease note product expiry dates are an indication only.");
	receipt.add("\tWellards accepts no liability for stated expiry dates,");
	receipt.add("\tplease refer to individual packaging to confirm use by and best before dates.");
        
        return receipt;
    }
}
