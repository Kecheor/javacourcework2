package ReceiptAfter;

public class CreditCard{
    
    String Type;
    String Number;
    
    public CreditCard(String type, String number)
    {
        Type = type;
        Number = number;
    }
    
    public String Last4Digits()
    {
        return Number.substring(Number.length()-4, Number.length());
    }
}
