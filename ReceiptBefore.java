import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReceiptBefore {
	public static void main2(String[] args) {
		List<String> receipt = new ArrayList<String>();
		List<String> list = new ArrayList<String>();
		//read file in
		try {
			Scanner in = new Scanner(new FileReader("receipt info.txt"));
			
			while(in.hasNextLine()) {
				String line = in.nextLine();
				list.add(line);
			}
			in.close();
		} catch (IOException e) {
			System.err.format("Error reading from file: %s%n", e);
			return;
		}
		List<String> orderDetails = list.subList(0, 11);
		List<String> goods = list.subList(14, list.size());
		
		double goodsTotal = 0;
		for (String line : goods) {
			String[] ary = line.split(",");
			
			goodsTotal += Double.valueOf(ary[3]);
		}
		String[] voucherLine = orderDetails.get(3).split(": ");
		String[] deliveryLine = orderDetails.get(4).split(": ");
		String[] cardLine = orderDetails.get(5).split(": ");
		String[] card = cardLine[1].split(" ");
		
		double pAndP = Double.valueOf(deliveryLine[1]);
		double voucher = Double.valueOf(voucherLine[1]);
		double total = goodsTotal + pAndP + voucher;
		
		//write out the receipt starting with the header
		receipt.add("\t\t\tYour receipt from Wellard's organic supermarket");
		receipt.add("");
		receipt.add("");

		//add customer details
		String nameLine = orderDetails.get(0);
		String[] tmp = nameLine.split(": ");
		String[] name = tmp[1].split(" "); //["smith,", "sarah", "ms"]
		
		String surname = name[0];
		surname = surname.substring(0, surname.length()-1); //remove ',' from end of name
		
		receipt.add( String.format("%s %s %s", name[2], name[1], surname) );
		receipt.add(orderDetails.get(1)); //add the 'order number' line as is
		receipt.add(orderDetails.get(2)); //same with the 'delivery date' line
		receipt.add("Customer Service team: 0345 000 000 (open 8am to 11pm every day) or");
		receipt.add("wellards@wellards.com");
		
		
		receipt.add("");
		receipt.add("");

		//add breakdown of costs and the total
		receipt.add( String.format("%-30s\t\t£%.2f","Cost of goods", goodsTotal) );
		receipt.add( String.format("%-30s\t\t£%.2f", "Picking, packing and delivery", pAndP) );
		receipt.add( String.format("%-30s\t\t-£%.2f", voucherLine[0], Math.abs(voucher)) );
		receipt.add( String.format("%-30s\t\t£%.2f", "Total charge", total) );
		receipt.add( String.format("%s%nCard: %s Last four digits: %s", "Payment details", card[0], card[4]) );
		receipt.add("");
		receipt.add("");

		
		//add itemised goods bill
		receipt.add( String.format("%-32s%s\t%s\t%s","Item","Quantity","Price","Price to pay (£)") );
		for (String s : goods) {
			String[] ary = s.split(",");
			receipt.add( String.format("%-32s%s\t\t%s\t%s", ary[0], ary[1], ary[2], ary[3]) );
		}
		
		//add footer
		receipt.add("");
		receipt.add("");
		receipt.add("\tPlease note product expiry dates are an indication only.");
		receipt.add("\tWellards accepts no liability for stated expiry dates,");
		receipt.add("\tplease refer to individual packaging to confirm use by and best before dates.");
		

		//write out to file
		try {
			FileWriter writer =  new FileWriter("formatted receipt.txt");
			for (int i = 0; i < receipt.size(); i++) {
				writer.write(String.format("%s%n", receipt.get(i)));
			}
			writer.close();
		} catch (IOException e) {
			System.err.format("Error writing receipt to disk: %s%n", e);
			return;
		}
	}
}
